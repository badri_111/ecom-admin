@extends('admin/layout')
@section('page_title','Manage coupon')
@section('container')
@section('coupon_select','active')
<h1 class='mb10'>Manage Coupon</h1>
<a href="{{url('admin/coupon')}}">
<button type="button" class="btn btn-success">Back</button>
</a>

<div class="row m-t-30">
                            <div class="col-md-12">
                             
                                <!-- DATA TABLE-->
                                <div class="table-responsive m-b-40">
                                <div class="row">
                              <div class="col-lg-12">
                                <div class="card">
                                
                                    <div class="card-body">
                           
                                        <form action="{{route('coupon.manage_coupon_process')}}" method="POST" >
                                            @csrf
                                            <div class="form-group">
                                                  <div class="row">  
                                                   <div class="col-lg-6">
                                             
                                                <label for="title" class="control-label mb-1">Coupon</label>
                                                <input id="title" name="title" value="{{$title}}" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                                  @error('title')   
                                                  <div class="alert alert-danger" role="alert">
                                                  {{$message}}
                                                    </div>
                            
                                                  @enderror
                                                 </div>
                                            
                                     
                                                <div class="col-lg-6">
                                          
                                                <label for="code" class="control-label mb-1">Code</label>
                                                <input id="code" name="code" value="{{$code}}" type="text" class="form-control cc-name valid" required>
                                                @error('code')   
                                                  <div class="alert alert-danger" role="alert">
                                                  {{$message}}
                                                    </div>
                            
                                                  @enderror
                                                </div>
                                               </div>
                                            </div>

                                            <div class="form-group">
                                                  <div class="row">  
                                                   <div class="col-lg-6">
                                             
                                                <label for="value" class="control-label mb-1">Value</label>
                                                <input id="value" name="value" value="{{$value}}" type="text" class="form-control cc-name valid" required>
                                                @error('value')   
                                                  <div class="alert alert-danger" role="alert">
                                                  {{$message}}
                                                    </div>
                            
                                                  @enderror
                                            </div>
                                            <div class="col-lg-6">
                                             
                                             <label for="type" class="control-label mb-1">Type</label>
                                           <select id="type" name="type" type="text" class="form-control cc-name valid"  >
                                                 
                                                  @if($type=='value')
                                                    <option selected value="value">value</option>
                                                   
                                                    @elseif($type=='per')
                                                    <option selected value="per">per</option>
                                                 
                                                    @else
                                                    <option value="value">value</option>
                                                    <option value="per">per</option>
                                                    
                                                    @endif
                                                    </select>
                                                 </div>
                                               </div>
                                            </div>
                                            <div class="form-group">
                                                  <div class="row">  
                                                   <div class="col-lg-6">
                                             
                                                <label for="min_order_amt" class="control-label mb-1">Min Order Amt</label>
                                                <input id="min_order_amt" name="min_order_amt" value="{{$min_order_amt}}" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                                  @error('min_order_amt')   
                                                  <div class="alert alert-danger" role="alert">
                                                  {{$message}}
                                                    </div>
                            
                                                  @enderror
                                                 </div>
                                            
                                     
                                                <div class="col-lg-6">
                                          
                                                <label for="is_one_time" class="control-label mb-1">Is One Time</label>
                                                <select id="is_one_time" name="is_one_time" type="text" class="form-control cc-name valid" required>
                                                 
                                                  @if($is_one_time==0)
                                                    <option selected value="1">Yes</option>
                                                   
                                                    <option  value="0">No</option>
                                                 
                                                    @else
                                                    <option value="1">Yes</option>
                                                    <option selected value="0">No</option>
                                                    
                                                    
                                                    @endif
                                                    </select>
                                                </div>
                                               </div>
                                            </div>
                                          
                                            
                                               
                                            <div>
                                                <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                                  Submit
                                                </button>
                                            </div>
                                            <input type="hidden" name="id" value="{{$id}}"/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                                
                            </div>
                         </div>
                                <!-- END DATA TABLE-->
                            </div>
</div>
@endsection