@extends('admin/layout')
@section('page_title','customer details')
@section('container')

@section('customer_show_select','active')


                   @if(session()->has('message'))
                    <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">              										
                            {{session('message')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                    </div>
                    @endif

  
<h1 class="mb10">Customer</h1>


<div class="row m-t-30">
                            <div class="col-md-8">

                                <!-- DATA TABLE-->
                                <div class="table-responsive m-b-40">
                                    <table class="table table-borderless table-data3">
                                    
                                        <tbody>
                                            
                                           
                                           <tr>    
                                               <td><strong>Name</strong> </td>                                  
                                                <td>{{$customer_list->name}}</td>
                                               
                                         </tr>
                                         <tr>    
                                               <td><strong> email</strong></td>                                  
                                                <td>{{$customer_list->email}}</td>
                                               
                                         </tr>
                                         <tr>    
                                               <td> <strong>mobile<</strong></td>                                  
                                                <td>{{$customer_list->mobile}}</td>
                                               
                                         </tr>
                                       
                                         <tr>    
                                               <td> <strong>Address</strong></td>                                  
                                               <td>{{$customer_list->address}}</td>
                                               
                                         </tr>
                                         <tr>    
                                               <td> <strong>City</strong></td>                                 
                                               <td>{{$customer_list->city}}</td>
                                               
                                         </tr>
                                         <tr>    
                                               <td> <strong>State</strong></td>                                  
                                               <td>{{$customer_list->state}}</td>
                                               
                                         </tr>
                                         <tr>    
                                               <td> <strong>Pincode<</strong>/td>                                  
                                               <td>{{$customer_list->pincode}}</td>
                                               
                                         </tr>
                                         <tr>    
                                               <td> <strong>Company</strong></td>                                  
                                               <td>{{$customer_list->company}}</td>
                                               
                                         </tr>
                                         <tr>    
                                               <td><strong> GST Number</strong></td>                                  
                                               <td>{{$customer_list->gstin}}</td>
                                               
                                         </tr>
                                         <tr>    
                                               <td> <strong>Created ON</strong></td>                                  
                                               <td>{{\carbon\carbon::parse($customer_list->created_at)
                                                   ->format('d-m-y h:i')}}</td>
                                               
                                         </tr>
                                         <tr>    
                                               <td><strong> Updated On</strong></td>                                  
                                               <td>{{\carbon\carbon::parse($customer_list->updated_at)
                                                   ->format('d-m-y h:i')}}</td>
                                               
                                         </tr>
                                         
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE-->
                            </div>
                        </div>
@endsection