<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Admin;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    //
    function index(Request $request)
    {

        $user =  $request->session()->get('admin_login');
        if ($user) {
            return redirect('/admin/dashboard');
        }
        else {
            return view('admin.login');
        }
    }
    function auth(Request $request)
    {
        $email=$request->post('email');
        $password=$request->post('password');
        $result=Admin::where(['email'=>$email])->first();
        if($result){
            if(Hash::check($password,$result->password))
            {
                $request->session()->put('admin_login',true);
                $request->session()->put('admin_id',$result->id);
                return redirect('admin/dashboard');
            }else{
                $request->session()->flash('error','please enter correct password');
                return redirect('/admin/login');
            }
            

        }else{
            $request->session()->flash('error','please enter valid login details');
            return redirect('/admin/login');
        }
    }
    function dashboard()
    {
        return view('admin.dashboard');

    }
    
}






