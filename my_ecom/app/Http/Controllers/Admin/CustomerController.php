<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Customer;

class CustomerController extends Controller
{
    //
    function index()
    {
        $result['data']=Customer::all();
        return view('admin.customer',$result);

    }
    function show(Request $req,$id='')
    {
        if($id>0){
        $arr=Customer::where(['id'=>$id])->get();
       
       $result['customer_list']=$arr['0'];
       
        return view('admin.customer_show', $result);
        }
        
    }

   
  
    function status(Request $req, $status,$id)
    {
        $model=Customer::find($id);
        $model->status=$status;
        $model->save();
        $req->session()->flash('message', 'Customer status  updated');
        return redirect('admin/customer');
    }
    
    
}
