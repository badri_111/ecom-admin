<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Size;
class SizeController extends Controller
{
    //
    function index()
    {
        $result['data']=Size::all();
        return view('admin.size',$result);

    }
    function manage_size(Request $req,$id='')
    {
        if($id>0){
        $arr=Size::where(['id'=>$id])->get();
        $result['size']=$arr['0']->size;
        $result['status']=$arr['0']->status;
        $result['id']=$arr['0']->id;
        }else{
            $result['size']='';
            $result['status']='';
            $result['id']='';

        }
       
        return view('admin.manage_size', $result);
        
    }
    function manage_size_process(Request $req)
    {
        //return $req->post();
         $req->validate( [
               
                'size'=>'required|unique:sizes,size,'.$req->post('id'),

             ]);
     
         if($req->post('id')>0){
             $model=Size::find($req->post('id'));
             $msg='size updated';
             
         }else{
            $model= new Size();
            $msg='Size inserted';
         }
         $model->size=$req->post('size');
         $model->status=1;
         $model->save();
          
         $req->session()->flash('message', $msg);
         return redirect('admin/size');

      
    }
     public function delete(Request $req,$id)
    {
      $model=Size::find($id);
      $model->delete();
      $req->session()->flash('message', 'Size deteted');
      return redirect('admin/size');

    }
    public function status(Request $req, $status,$id)
    {
        $model=Size::find($id);
        $model->status=$status;
        $model->save();
        $req->session()->flash('message', 'Size status  updated');
        return redirect('admin/size');
    }
    
    
}
