<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Brand;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BrandController extends Controller
{
    //
    function index()
    {
        $result['data']=Brand::all();
        return view('admin.brand',$result);

    }
    function manage_brand(Request $req,$id='')
    {
        if($id>0){
        $arr=Brand::where(['id'=>$id])->get();
        $result['name']=$arr['0']->name;
        $result['image']=$arr['0']->image;
        $result['is_home']=$arr['0']->is_home;
        $result['is_home_selected']=$arr['0']->is_home_selected;
        if($arr['0']->is_home==1){
            $result['is_home_selected']="checked";
        }
        $result['id']=$arr['0']->id;
        }else{
            $result['name']='';
            $result['image']='';
            $result['is_home']=$arr['0']='';
            $result['is_home_selected']='';
            $result['id']='';

        }
       
        return view('admin.manage_brand', $result);
        
    }
    function manage_brand_process(Request $request)
    {
        //return $req->post();
       
  
           $request->validate( [
                  
                  'name'=>'required|unique:brands,name,'.$request->post('id'),

                  'image'=>'mimes:jpeg,jpg,png,jfif'
                   
               ]);

               
         
     
         if($request->post('id')>0){
             $model=Brand::find($request->post('id'));
             $msg='brand updated';
             
         }else{
            $model= new Brand();
            $msg='Brand inserted';
         }

        
         if($request->hasfile('image')){

            if($request->post('id')>0){
                $arrImage=DB::table('brands')->where(['id'=>$request->post('id')])->get();
                if(Storage::exists('/public/media/brand/'.$arrImage[0]->image)){
                    Storage::delete('/public/media/brand/'.$arrImage[0]->image);
                }
            }

            $image=$request->file('image');
            $ext=$image->extension();
            $image_name=time().'.'.$ext;
            $image->storeAs('/public/media/brand',$image_name);
            $model->image=$image_name;
        }
        $model->is_home=0;
        if($request->post('is_home')!==null){
            $model->is_home=1;
        }
        $model->name=$request->post('name');
        $model->status=1;
        $model->save();
        $request->session()->flash('message',$msg);
        return redirect('admin/brand');
        

      
    }
     public function delete(Request $req,$id)
    {
      $model=Brand::find($id);
      $model->delete();
      $req->session()->flash('message', 'brand deteted');
      return redirect('admin/brand');

    }
    public function status(Request $req, $status,$id)
    {
        $model=Brand::find($id);
        $model->status=$status;
        $model->save();
        $req->session()->flash('message', 'brand status  updated');
        return redirect('admin/brand');
    }
    
    
}
