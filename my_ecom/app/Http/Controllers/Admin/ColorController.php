<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Color;
use App\Http\Controllers\Controller;
class ColorController extends Controller
{
    //
    function index()
    {
        $result['data']=Color::all();
        return view('admin.color',$result);

    }
    function manage_color(Request $req,$id='')
    {
        if($id>0){
        $arr=Color::where(['id'=>$id])->get();
        $result['color']=$arr['0']->color;
        $result['status']=$arr['0']->status;
        $result['id']=$arr['0']->id;
        }else{
            $result['color']='';
            $result['status']='';
            $result['id']='';

        }
       
        return view('admin.manage_color', $result);
        
    }
    function manage_color_process(Request $req)
    {
        //return $req->post();
         $req->validate( [
               
                'color'=>'required|unique:sizes,size,'.$req->post('id'),

             ]);
     
         if($req->post('id')>0){
             $model=Color::find($req->post('id'));
             $msg='color updated';
             
         }else{
            $model= new Color();
            $msg='color inserted';
         }
         $model->color=$req->post('color');
         $model->status=1;
         $model->save();
          
         $req->session()->flash('message', $msg);
         return redirect('admin/color');

      
    }
     public function delete(Request $req,$id)
    {
      $model=Color::find($id);
      $model->delete();
      $req->session()->flash('message', 'color deteted');
      return redirect('admin/color');

    }
    public function status(Request $req, $status,$id)
    {
        $model=Color::find($id);
        $model->status=$status;
        $model->save();
        $req->session()->flash('message', 'color status  updated');
        return redirect('admin/color');
    }
    
}
