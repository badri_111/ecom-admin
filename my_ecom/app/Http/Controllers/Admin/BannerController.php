<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
    //
    function index()
    {
        $result['data']=Banner::all();
        return view('admin.banner',$result);
    }
   function manage_banner(Request $req,$id='')
    {
        if($id>0){
        $arr=Banner::where(['id'=>$id])->get();
        $result['btn_text']=$arr['0']->btn_text;
        $result['btn_link']=$arr['0']->btn_link;
        $result['image']=$arr['0']->image;
        $result['id']=$arr['0']->id;
        }else{
            $result['btn_text']='';
            $result['btn_link']='';
            $result['image']='';

            $result['id']='';

        }
       
        return view('admin.manage_banner', $result);
        
    }
    function manage_banner_process(Request $request)
    {
        //return $req->post();
       
  
           $request->validate( [
                  
                 
                  'image'=>'mimes:jpeg,jpg,png,jfif'
                   
               ]);

               
         if($request->post('id')>0){
             $model=Banner::find($request->post('id'));
             $msg='banner updated';
             
         }else{
            $model= new Banner();
            $msg='banner inserted';
         }

        
         if($request->hasfile('image')){

            if($request->post('id')>0){
                $arrImage=DB::table('banners')->where(['id'=>$request->post('id')])->get();
                if(Storage::exists('/public/media/banner/'.$arrImage[0]->image)){
                    Storage::delete('/public/media/banner/'.$arrImage[0]->image);
                }
            }

            $image=$request->file('image');
            $ext=$image->extension();
            $image_name=time().'.'.$ext;
            $image->storeAs('/public/media/banner',$image_name);
            $model->image=$image_name;
        }
       
        $model->btn_text=$request->post('btn_text');
        $model->btn_link=$request->post('btn_link');
    
        $model->status=1;
        $model->save();
        $request->session()->flash('message',$msg);
        return redirect('admin/banner');
        

      
    }
     public function delete(Request $req,$id)
    {
      $model=Banner::find($id);
      $model->delete();
      $req->session()->flash('message', 'banner deteted');
      return redirect('admin/brand');

    }
    public function status(Request $req, $status,$id)
    {
        $model=Banner::find($id);
        $model->status=$status;
        $model->save();
        $req->session()->flash('message', 'banner status  updated');
        return redirect('admin/banner');
    }
    
}
