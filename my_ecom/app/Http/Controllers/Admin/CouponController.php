<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Coupon;

class CouponController extends Controller
{
    //
    public function index()
    {
        $result['data']=Coupon::all();
        return view('admin.coupon',$result);

    }
    function manage_coupon(Request $req,$id='')
    {
        if($id>0){
        $arr=Coupon::where(['id'=>$id])->get();
        $result['title']=$arr['0']->title;
        $result['code']=$arr['0']->code;
        $result['value']=$arr['0']->value;
        $result['type']=$arr['0']->type;
        $result['min_order_amt']=$arr['0']->min_order_amt;
        $result['is_one_time']=$arr['0']->is_one_time;
        $result['id']=$arr['0']->id;
        }else{
            $result['title']='';
            $result['code']='';
            $result['value']='';
            $result['type']='';
            $result['min_order_amt']='';
            $result['is_one_time']='';
            $result['id']='';

        }
       
        return view('admin.manage_coupon', $result);
        
    }
    function manage_coupon_process(Request $req)
    {
        //return $req->post();
         $req->validate( [
                'title'=>'required',
                'code'=>'required|unique:coupons,code,'.$req->post('id'),
                'value'=>'required',

             ]);
     
         if($req->post('id')>0){
             $model=Coupon::find($req->post('id'));
             $msg='coupon updated';
         }else{
            $model= new Coupon();
            $msg='coupon inserted';
            $model->status=1;
         }
         $model->title=$req->post('title');
         $model->code=$req->post('code');
         $model->value=$req->post('value');
         $model->type=$req->post('type');
         $model->min_order_amt=$req->post('min_order_amt');
         $model->is_one_time=$req->post('is_one_time');
         $model->save();
          
         $req->session()->flash('message', $msg);
         return redirect('admin/coupon');

      
    }
     public function delete(Request $req,$id)
    {
      $model=Coupon::find($id);
      $model->delete();
      $req->session()->flash('message', 'coupon deteted');
      return redirect('admin/coupon');

    }
    public function status(Request $req, $status,$id)
    {
        $model=Coupon::find($id);
        $model->status=$status;
        $model->save();
        $req->session()->flash('message', 'coupon status  updated');
        return redirect('admin/coupon');
    }
}
