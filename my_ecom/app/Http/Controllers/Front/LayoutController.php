<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class LayoutController extends Controller
{
    //
    public function index(Request $request){

        $result['parent_categories'] =DB::table('categories')
        ->where(['status'=>1])
        ->where(['parent_category_id'=>0])
        ->get();

$result['categories_menu']=DB::table('categories')
        ->where(['status'=>1])
        ->get();

foreach($result['categories_menu'] as $category) {
    if($category->parent_category_id == 0) {
        $result[$category->category_name]=DB::table('categories')
        ->where(['status'=>1])
        ->where(['parent_category_id'=>$category->id])
        ->get();
    }
}
    return view('front.layout',$result);
    }
}
